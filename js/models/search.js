/**
* Certificacion Model
* version : 1.0
* package: Coaching
* author: Jhonny gil
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	SearchModel = Backbone.Model.extend({

	        defaults: {
		       nombre: '',
		       apellido: '',
		       cedula: '',
		       mail: '',
		       mensaje: '',
		       fecha: ''     
	        }

	    ,   initialize: function(){

	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	sendMail: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_inscripciones'
					,	task: 'inscripciones.sendMail'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );