/**
* Compartir View
* version : 1.0
* package: Universidad santo tomas
* author: Jhonny Gil
* Creation date: Enero 2015
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var SearchView = {};


	// Extends my object from Backbone events
	SearchView = Backbone.View.extend({
			el: $( 'body' )

		,	events: {
				'submit #form_advanced_search': 'buscar'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'buscar'
				);

				$("#temas_buscador").select2();
				$("#tipo_recurso").select2();
				$("#palabras_claves").select2();
			}
		
		,	buscar : function(e){
				preventDefault(e);
				console.log("sdfd");
				alert("algo");
		}
				

		,	onSendEmailForm: function( data ){

				console.log( data );

				if(data.status == 500)
					utilities.showNotification( 'error', data.message, 0 );

				utilities.showNotification( 'success', data.message, 0 );

			}						

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.SearchView = new SearchView();

})( jQuery, this, this.document, this.Misc, undefined );