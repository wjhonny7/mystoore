<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
    	<meta charset="utf-8" />
        <meta content="telephone=no" name="format-detection">
		<jdoc:include type="head" />

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Coda:400,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=template-commerce">
        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <!-- Scripts -->
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery-ui.js"></script>        
	</head>

    <body>
        <header>
            <div class="header-centered">
                <div class="login-top">
                    <jdoc:include type="modules" name="login-top" style="xhtml"/>
                </div>

                <div class="logo-stoore">
                    <img src="images/inicio/logo.jpg">
                </div>

                <nav>
                    <jdoc:include type="modules" name="menu" style="xhtml"/>
                </nav>

                <div class="shopping-cart">
                    <jdoc:include type="modules" name="shopping-cart" style="xhtml"/>
                </div>
            </div>
        </header>

        <div class="redes-lateral">
            <div class="fb">
                <jdoc:include type="modules" name="fb" style="xhtml"/>
            </div>
            <div class="tw">
                <jdoc:include type="modules" name="tw" style="xhtml"/>
            </div>
            <div class="yb">
                <jdoc:include type="modules" name="yb" style="xhtml"/>
            </div>
        </div>

        <div class="contacto">
            <jdoc:include type="modules" name="contacto" style="xhtml"/>
        </div>

        <main>
                <div class="search">
                     <jdoc:include type="modules" name="search" style="xhtml"/>
                </div>
            <jdoc:include type="message" />
            <jdoc:include type="component" />
        </main>

        <footer>
            <div class="footer-centered">
                <div class="footer-top">
                    <jdoc:include type="modules" name="redes-bottom" style="xhtml"/>
                    <jdoc:include type="modules" name="datos-bottom" style="xhtml"/>
                </div>
                <div class="footer-middle">
                     <jdoc:include type="modules" name="mapa-sitio" style="xhtml"/>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-bottom-centered">
                    <jdoc:include type="modules" name="direccion" style="xhtml"/>
                    <div class="copy">
                        <span class="sainet">
                            <a target="_blank" href="http://www.creandopaginasweb.com">
                                Página web diseñada por <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logo_blanco.png">
                            </a>
                        </span>
                    </div>
                </div>
            </div>   
        </footer>
        <script type="text/javascript" src="js/load-scripts.php"></script>
	</body>    
</html>
<?php 
    if(  $_GET["acceso"]=="mal" && $_SESSION['__default']['user']->id==0){
?>
<script type="text/javascript">
    alert("Usuario o contraseña incorrectos.");
</script>
<?php 
    }
?>
