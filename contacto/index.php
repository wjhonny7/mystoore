<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Cagliostro' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
<script src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
		
				function valida_envia(){
					
					if(document.fvalida.nombre.value.length==0){
						alert("Por Favor escriba su nombre");
						document.fvalida.nombre.focus();
						return false;
					}
				
					
					if(document.fvalida.email.value.indexOf('@')==-1){
					alert("La direccion de email no es correcta");
					document.fvalida.email.focus();
					return false;
					}
					
					if(document.fvalida.mensaje.value.length==0){
						alert("Por Favor escriba su mensaje");
						document.fvalida.mensaje.focus();
						return false;
					}
					
					
					
					document.fvalida.submit();
					
					
				}
</script>

<style>


* {
	color: #0071ba;
	text-align: left;
	font-family: 'Open Sans', sans-serif;
	font-weight: normal;
	font-size: 17px;
}

.error { color:#C00;  }

em { color:#C00; }

h2 {
    font-family: 'Open Sans', sans-serif;
    font-size: 45px;
    border-bottom: 1px solid #E9D9C8;
    padding-bottom: 2px;
    margin-bottom: 10px;
    color: #333333;
}

input[type='text']{ 
    font-size: 14px;
	font-family: 'Open Sans', sans-serif;
    height: 30px;
	font-weight:normal;
    margin-bottom: 5px;
    margin-left: 1px;
    padding-bottom: 3px;
    padding-left: 10px;
    text-align: left;
    width: 400px;
	color:#999;
    border-radius: 3px;
    background-color: #fff;
    border:1px solid #ccc;
}

textarea{
	background-color: #fff;
	border:1px solid #ccc;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
	font-weight:normal;
    margin-bottom: 5px;
    margin-left: 1px;
    padding-bottom: 1px;
    text-align: left;
    width: 400px;
	height: 99px; !important;
	overflow:auto;
	color:#999;
    border-radius: 3px;
    padding-left: 10px; 
}

input[type='submit']{  
    background-color: #FF6628;
    border: medium none;
    color:#fff;
    cursor: pointer;
    font-family: 'Fjalla One', sans-serif;
    font-size: 14px;
    height:26px;
    padding-top: 0;
    text-align: center;
    width: 73px;
    margin-top: 1px;
    margin-left: 17px;
    margin-bottom: 5px;
    border-radius: 0px;
    -webkit-appearance: none;
}
    
input[type='submit']:hover {opacity:0.7;}


input[type='reset']{ 
    background-color: #FF6628;
    border: medium none;
    color:#fff;
    cursor: pointer;
    font-family: 'Fjalla One', sans-serif;
    font-size: 14px;
    height:26px;
    padding-top: 0;
    text-align: center;
    width: 73px;
    margin-top: 1px;
    margin-left: 17px;
    margin-bottom: 5px;
    border-radius: 5px;
    margin-left: 250px;
    border-radius: 0px;
    -webkit-appearance: none;  
}
    
input[type='reset']:hover {opacity:0.7;}

.dropdown {
    width: 411px;
    height: 30px;
    display: inline-block;
    background: url("select.png") no-repeat center right, #fff;
    overflow: hidden;
    position: relative;
    border-radius: 4px;
    border:1px solid #ccc;
    margin-bottom: 5px;

}

select {
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    float: left;
    width: 105%;
    height: 30px;
    padding-left: 6px;
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 0 none;
    border-radius: 4px;
    color: #ccc;
    font-size: @normalFont;
    font-family: @titlefont;
    line-height: 1;
    -webkit-appearance: none;
    appearance: none;
    -moz-appearance: none;
    cursor: pointer;
}

	
td {/*padding-bottom:12px;*/}

#commentForm { }

/*#commentForm label { width:114px; height:10px;}

#commentForm label.error { margin-left: 6px; }*/


</style>

<body>
<form id="commentForm"  name="fvalida" method="post" action="procesar.php" onSubmit="return valida_envia()">
<table style="width: 430px; height:350px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td><input name="nombre" type="text" id="cname" minlength="2" placeholder="Nombre" /></td>
</tr>
<tr>
<td><input name="email" type="text" id="cemail" class="required" placeholder="E-mail" /></td>
</tr>
<tr>
<td><input onkeypress="return justNumbers(event);" name="tel" type="text" id="tel" class="required" placeholder="Teléfono" /></td>
</tr>
<tr>
<td><input name="ciudad" type="text" id="ciudad" class="required" placeholder="Ciudad" /></td>
</tr>
<tr>
<td><textarea name="mensaje" cols="20" rows="3" id="mensaje" class="required" placeholder="Mensaje"></textarea></td>
</tr>
<tr>
<td><input type="reset" value=" Borrar"/><input type="submit" name="Submit" value="Enviar"  /></td>
</tr>
</tbody>
</table>
</form>

</body>
</head>
</html>




