<?php
 /**
 * ------------------------------------------------------------------------
 * JU LoadjQuery plugin for Joomla 2.5
 * ------------------------------------------------------------------------
 * Copyright (C) 2010-2012 JoomUltra. All Rights Reserved.
 * @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
 * Author: JoomUltra Co., Ltd
 * Websites: http://www.joomultra.com
 * ------------------------------------------------------------------------
 */
 
// No direct access.
defined('_JEXEC') or die;

class plgSystemJULoadjQuery extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	 public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}
		
	/**
	* @since	1.6
	*/
	public function onAfterRender()
	{
	
        $app = JFactory::getApplication();
        if ($this->params->get('enableplugin', '1')=='1' && $app->isAdmin()) return;
		if ($this->params->get('enableplugin', '1')=='2' && !$app->isAdmin()) return;
			
		$document =& JFactory::getDocument();
		$option = JRequest::getVar('option');
		$itemid = JRequest::getVar('Itemid');
		$front_option_list = $this->params->get('front_option_list', '');
		$front_itemid_list = $this->params->get('front_itemid_list', '');
		$front_option_list_arr = explode(",", str_replace(" ", "", $front_option_list));
		$front_itemid_list_arr = explode(",", str_replace(" ", "", $front_itemid_list));
		
		$back_option_list = $this->params->get('back_option_list', '');
		$back_option_list_arr = explode(",", str_replace(" ", "", $back_option_list));
		
		//Front-end check
		if(!$app->isAdmin()) {
			//Run plugin if in the list of pages and ENABLE
			if ( (in_array($option, $front_option_list_arr) || (!empty($itemid) && in_array($itemid, $front_itemid_list_arr)) ) && $this->params->get('front_mode','0') )
			{
				plgSystemJULoadjQuery::loadjQuery(); 
			}
			//Run plugin if in the NOT list of pages and DISABLE
			elseif ( !( in_array($option, $front_option_list_arr) || (!empty($itemid) && in_array($itemid, $front_itemid_list_arr)) ) && !$this->params->get('front_mode','0') )
			{
				plgSystemJULoadjQuery::loadjQuery();
			}
		//Back-end check
		} else {
			//Run plugin if in the list of pages and ENABLE
			if ( in_array($option, $back_option_list_arr) && $this->params->get('back_mode','0') )
			{
				plgSystemJULoadjQuery::loadjQuery(); 
			}
			//Run plugin if in the NOT list of pages and DISABLE
			elseif ( !in_array($option, $back_option_list_arr) && !$this->params->get('back_mode','0') )
			{
				plgSystemJULoadjQuery::loadjQuery();
			}
		}
		
	}
	private function loadjQuery()
	{
		if ($this->params->get('stripnoconflict','1')=='1')
		{
			plgSystemJULoadjQuery::stripnoConflict();
		}
		
		$loadjquery = $this->params->get('loadjquery','3');
		//Always load
		if($loadjquery==1)
		{
			plgSystemJULoadjQuery::addjQuery();
		}
		//Load jQuery when not exists
		elseif ($loadjquery==2)
		{
			if (!plgSystemJULoadjQuery::checkjQuery())
			{
				plgSystemJULoadjQuery::addjQuery();
			}
		}
		//Load jQuery and delete others
		else
		{
			plgSystemJULoadjQuery::removeOldThenAddNewjQuery();
		}
	}
	
	private function checkjQuery()
	{
		$body = JResponse::getBody();
		$regex= '#\<script.* src=\".*jquery([0-9\.-]|min|pack)*?.js\".*\>\<\/script\>#m';
		preg_match($regex, $body, $matches);
		if (empty($matches)) return FALSE;
		else return TRUE;
	}
	
	private function stripnoConflict()
	{
		$body = JResponse::getBody();
		
		if (preg_match_all('#(jQuery|\$)\.noConflict\((true|false|)\);#', $body, $matches, PREG_SET_ORDER) > 0) {
			foreach ($matches as $match) {					
				$quoted_match = preg_quote($match[0]);//echo $match[0]; exit;
				if (preg_match('#<script type="text/javascript">([^>]*)'.$quoted_match.'#', $body)) {
					$body = preg_replace('#'.$quoted_match.'#', '', $body, 1);
				}
			}
			$body = preg_replace('#<script type="text/javascript">\s*</script>#', '', $body, -1, $count);
		}
		
		JResponse::setBody($body);
		unset($body);
	}
	
	private function addjQuery()
	{
		$body	= JResponse::getBody();
		
		$protocol = $this->params->get('protocol','http');
		$jquery_version = $this->params->get('jquery_version','1.8');
		$jquery_subversion = $this->params->get('jquery_subversion','');
		$jqueryfile = $this->params->get('jqueryfile','jquery-1.8.2.min.js');
		
		//Load jQuery after mootools
		$js_needing_mootools = array("mootools-more.js","mootools-core.js","mooRainbow.js", "mootree.js");
		
		if ($jquery_subversion)	{ $jquery_subversion = '.'.$jquery_subversion; }
		
		if ($this->params->get('loadjqueryfrom','1')=="1")
		{
			$jqueryurl = 	JUri::root().'plugins/system/juloadjquery/jquery/'.$jqueryfile;
		}
		elseif ($this->params->get('loadjqueryfrom','1')=="2")
		{
			$jqueryurl = $protocol."://ajax.googleapis.com/ajax/libs/jquery/".$jquery_version.$jquery_subversion."/jquery.min.js";	
		}
		
		$javarscript[]	= '<script type="text/javascript" src="'.$jqueryurl.'"></script>';
		if ($this->params->get('noconflict','0'))
		{
			$javarscript[]	= '<script type="text/javascript">jQuery.noConflict();</script>';
		}
		
		$javarscript[]	= $this->params->get('custom_script','');
		
		$javarscript_str= implode("\n",$javarscript);
		
		foreach ($js_needing_mootools as $library) 
		{
			if (preg_match("#\<script.* src=\".*".$library."\".*\>\<\/script\>#m", $body, $matches)) 
			{
				if (!empty($matches)) break;
			}
		}

		$body = str_replace ($matches[0], $matches[0]."\n".$javarscript_str, $body);
		
		JResponse::setBody($body);
		unset($body);
	}
	
	private function removeOldThenAddNewjQuery()
	{ 
		$body	= JResponse::getBody();
		
		//Remove other exists jQuery
		if (plgSystemJULoadjQuery::checkjQuery())
		{
			preg_match_all("#\<script.* src=\".*jquery([0-9\.-]|min|pack)*?.js\".*\>\<\/script\>#m", $body, $matches); 
			$match_arr = $matches[0];
			foreach ($match_arr as $match) 
			{
				$body = str_replace ($match, "", $body);
			}
			JResponse::setBody($body);
			unset($body);
		}
		
		//Add jQuery from plugin
		plgSystemJULoadjQuery::addjQuery();
	}
}
